import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { Charts, Button, CheckBox, CheckBoxGroup, DropDownList, Error, GUIContainer, Image, InputFile, InputText, Label, RadioButton, RadioButtonGroup, TextEdit, TextView } from 'monolith-frontend';
// import GenericBubble from 'monolith-api-controller';

import { GenericBubble, WebNotification } from 'monolith-frontend';

// console.log(Button);
// const TextEdit = view.gui.

class TestBubble extends GenericBubble {
  aliveRender() {
    return (
      <p>
        <Button url="www.google.it" text="I am alive!" />
      </p>
    );
  }

  notAliveRender() {
    return (
      <p>
        <Button text="I am dead!" />
      </p>
    );
  }


}

import '../node_modules/monolith-frontend/dist/css/main.css';

// const buttons = [{
//     name: 'A',
//     value: 'A',
//     label: 'Aaaa',
//     checked: true,
//   },
//   {
//     name: 'B',
//     value: 'B',
//     label: 'Bbbb',
//   },
//   {
//     name: 'C',
//     value: 'C',
//     label: 'Ccc',
//   },
//   {
//     name: 'D',
//     value: 'D',
//     label: 'Dd',
//     checked: true,
//   }
// ];

// const errorListValues = [
//     {
//         label: 'Ananas',
//         optionValue: 'ananas',
//     },
//     {
//         label: 'Banana',
//         optionValue: 'banana',
//         selected: true,
//     },
//     {
//         label: 'Cherry',
//         optionValue: 'banana',
//         selected: true,
//     }
// ];

const listValues = [
    {
        label: 'Ananas',
        optionValue: 'ananas',
    },
    {
        label: 'Banana',
        optionValue: 'banana',
        selected: true,
    },
    {
        label: 'Cherry',
        optionValue: 'cherry',
        selected: true,
    }
];

const chartData = [
    {name: 'Page A', uv: 4000, pv: 2400, amt: 2400},
    {name: 'Page B', uv: 3000, pv: 1398, amt: 2210},
    {name: 'Page C', uv: 4000, pv: 9800, amt: 2290},
    {name: 'Page D', uv: 2780, pv: 3908, amt: 2000},
    {name: 'Page E', uv: 1890, pv: 4800, amt: 2181},
    {name: 'Page F', uv: 2390, pv: 3800, amt: 2500},
    {name: 'Page G', uv: 3490, pv: 4300, amt: 2100},
];

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: 'Initial TextView \r\n text',
        };
    }

    onTextChange(data) {
        console.log(data);
        new WebNotification('web', data.toString(), '').notify();
        this.setState({
            text: data,
        });
    }

  render() {
    return (
      <div className="App">
        {/*<div className="App-header">*/}
          {/*<img src={logo} className="App-logo" alt="logo" />*/}
          {/*<h2>Welcome to React</h2>*/}
        {/*</div>*/}
        {/*<p className="App-intro">*/}
          {/*To get started, edit <code>src/App.js</code> and save to reload.*/}
        {/*</p>*/}
        {/*<Button url="http://www.google.it" text="button versione anchor"/>*/}
        {/*<Button text="button versione button" />*/}
        {/*<CheckBox name="checkTest" value="checkTest" label="test"/>*/}

          {/*{Charts.BarChartFactory.createBarChart(chartData, { width: 600, height: 300 }, { xAxisDataKey: 'name', yAxisDataKeys: [*/}
            {/*'uv', 'pv', 'amt'*/}
          {/*]})}*/}
        <TestBubble url="http://localhost:3456" time={5} />


        <InputText id="test" onTextChange={newValue => this.onTextChange(newValue)} />
          <div>
            <DropDownList values={listValues} />
          </div>
          {/*<DropDownList values={errorListValues} />*/}
        <GUIContainer>
            {/*<Image src="https://unsplash.it/200/300/?random" caption="random"/>*/}
            {/*<CheckBoxGroup name="checkBoxGroup" buttons={buttons}/>*/}
            {/*<RadioButtonGroup name="radioGroup" buttons={buttons}/>*/}
            {/*<TextEdit name="test" value="testsss" onTextChange={(newValue) => this.onTextChange(newValue)}/>*/}
            {/*<TextView text={this.state.text}/>*/}
            {/*<Error errorMessage="test" />*/}
        </GUIContainer>
      </div>
    );
  }
}

{/*<BarChart width={600} height={300} data={chartData}>*/}
{/*<XAxis dataKey="name"/>*/}
{/*<YAxis/>*/}
{/*<CartesianGrid strokeDasharray="3 3"/>*/}
{/*<Tooltip/>*/}
{/*<Legend />*/}
{/*<Bar dataKey="pv" fill="#8884d8" />*/}
{/*<Bar dataKey="uv" fill="#82ca9d" />*/}
{/*</BarChart>*/}

export default App;
